import { NextFunction, Request, Response } from 'express';
import HttpError from 'http-errors';
import { MongoServerError } from 'mongodb';

const mongoServerErrorHandler = (err: any) => {
    const mongoServerError = err as MongoServerError
    let status = 500
    let message = 'Mongo server error.'
    
    if (mongoServerError.code === 11000) {
        status = 400
        message = err.name + ': ' + err.message
    }

    return HttpError(status, message)
}

export const errorHandler = (err: any, req: Request, res: Response, next: NextFunction) => {
    if (err.name === 'MongoServerError') {
        err = mongoServerErrorHandler(err)
    }

    const status = err.status || 500
    const message = err.message || 'Internal server error.'
    
    res.status(status).send({ error: message })
} 