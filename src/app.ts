import dotenv from 'dotenv';
import express from 'express';
import morgan from 'morgan';
import cors from 'cors';

// Environment import and configuration
dotenv.config()

import { initializeDBConnection } from './db';
import { errorHandler } from './middlewares/error-handler';
import { mountRoutes } from './routes';


// Express configuration
const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(morgan('tiny'))
app.use(cors())

// Server configuration
const PORT = process.env.PORT || 8001

// Initiate MongoDB Atlas connection
initializeDBConnection()

// Initialize application routes
mountRoutes(app)

// HTTP(S) error handler
app.use(errorHandler)

// Listening to requests
app.listen(PORT, () => { console.log('Listening...'); })