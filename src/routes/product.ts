import { Router } from 'express';
import { ProductInterface } from '../interfaces/Product';
import { Product } from '../models/Product';
import { SKUGenerator } from '../utils/sku-generation';
import HttpError from 'http-errors';

const router: Router = new (Router as any)()

/**
 * @route POST /product
 * @desc Create a new product
 */
router.post('/', async (req, res, next) => {
    try {
        const { title } = req.body
        const product = { _id: SKUGenerator(), title } as ProductInterface

        await Product.create(product)

        return res.status(201).send()
    } catch (error) {
        next(error)
    }
})

/**
 * @route /products
 * @desc Get all products
*/
router.get('/list', async (req, res, next) => {
    const products = await Product.find()

    return res.send(products.map(product => product.toObject({ versionKey: false })))
})

/**
 * @route GET /product
 * @desc Get a product by the provided parent SKU
 */
 router.get('/:parent_sku', async (req, res, next) => {
    try {
        const { parent_sku } = req.params

        if (!parent_sku) {
            throw HttpError(400, 'Parent SKU is missing!')
        }

        const product = await Product.findById(parent_sku)

        if (!product) {
            throw HttpError(404, 'We could not find a product with the given parent SKU!')
        }

        return res.send(product.toObject({ versionKey: false }))
    } catch (error) {
        next(error)
    }
})

/**
 * @route POST /product
 * @desc Update an existing product
 */
 router.put('/', async (req, res, next) => {
    try {
        const { parent_sku, title } = req.body

        if (!parent_sku) {
            throw HttpError(400, 'Parent SKU is missing!')
        }

        const product = await Product.findById(parent_sku)

        if (!product) {
            throw HttpError(404, 'We could not find a product with the given parent SKU!')
        }

        await product.updateOne({ title })

        return res.status(204).send()

        // GENERAL APPROACH
        // const product = { _id: parent_sku, title } as ProductInterface

        // const oldProduct = await Product.findById(parent_sku)

        // if (!oldProduct) {
        //     throw HttpError(400, 'We could not find a product with the given parent SKU!')
        // }

        // const _product = Object.fromEntries(Object.entries(product).filter(([_, value]) => value != null)) as ProductInterface
        // const _oldProduct = Object.fromEntries(Object.entries(oldProduct.toObject()).filter(
        //     ([key]) => [ '_id', 'title' ].includes(key)
        // )) as ProductInterface

        // const newProduct = { ..._oldProduct, ..._product } as ProductInterface
    } catch (error) {
        next(error)
    }
})

/**
 * @route DELETE /product
 * @desc Delete an existing product
 */
 router.delete('/', async (req, res, next) => {
    try {
        const { parent_sku } = req.body

        if (!parent_sku) {
            throw HttpError(400, 'Parent SKU is missing!')
        }

        const product = await Product.findById(parent_sku)

        if (!product) {
            throw HttpError(404, 'We could not find a product with the given parent SKU!')
        }

        await product.delete()

        return res.send()
    } catch (error) {
        next(error)
    }
})

export { router };

