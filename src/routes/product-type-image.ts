import { Router } from "express";
import HttpError from 'http-errors';
import { ImageInterface } from "../interfaces/Image";
import { ProductType } from "../models";

const router: Router = new (Router as any)()

/**
 * @route POST /product/type/image
 * @desc Add a new image to a given product type
 */
 router.post('/', async (req, res, next) => {
    try {
        const { sku, source, label } = req.body
        const productTypeImage = { source, label } as ImageInterface

        if (!sku) {
            throw HttpError(400, 'SKU is missing!')
        }

        const productType = await ProductType.findById(sku)
        
        if (!productType) {
            throw HttpError(400, 'We could not find a product type with the given SKU!')
        }

        productType.images.push(productTypeImage)
        await productType.save()

        res.status(201).send()
    } catch (error) {
        next(error)
    }
})

/**
 * @route PUT /product/type/image
 * @desc Update an existing image of a product type
 */
router.put('/', async (req, res, next) => {
    try {
        const { sku, imageID, source, label } = req.body
        const productTypeImage = { source, label } as ImageInterface

        if (!sku) {
            throw HttpError(400, 'SKU is missing!')
        }

        const productType = await ProductType.findById(sku)    

        if (!productType) {
            throw HttpError(400, 'We could not find a product type with the given SKU!')
        }

        const image = productType.images.find(image => image._id == imageID)

        if (!image) {
            throw HttpError(400, 'The searched product type does not have the image with the given id!')
        }

        await ProductType.findOneAndUpdate(
            { "_id": sku, "images._id": imageID },
            { "images.$": { ...productTypeImage, _id: imageID } }
        )

        res.status(204).send()
    } catch (error) {
        next(error)
    }
})

/**
 * @route DELETE /product/type/image
 * @desc Delete an existing image of a product type
 */
 router.delete('/', async (req, res, next) => {
    try {
        const { sku, imageID } = req.body

        if (!sku) {
            throw HttpError(400, 'SKU is missing!')
        }

        const productType = await ProductType.findById(sku)    

        if (!productType) {
            throw HttpError(400, 'We could not find a product type with the given SKU!')
        }

        const image = productType.images.find(image => image._id == imageID)

        if (!image) {
            throw HttpError(400, 'The searched product type does not have the image with the given id!')
        }

        await productType.updateOne({ $pull: { images: { _id: imageID } } })

        res.send()
    } catch (error) {
        next(error)
    }
})

export { router };