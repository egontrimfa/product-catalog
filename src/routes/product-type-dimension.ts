import { Router } from "express";
import HttpError from 'http-errors';
import { DimensionInterface } from "../interfaces/Dimension";
import { ProductType } from "../models";

const router: Router = new (Router as any)()

/**
 * @route POST /product/type/dimension
 * @desc Add the dimensions to a given product type
 */
 router.post('/', async (req, res, next) => {
    try {
        const { sku, width, length, height, unit } = req.body
        const productTypeDimension = { width, length, height, unit } as DimensionInterface

        if (!sku) {
            throw HttpError(400, 'SKU is missing!')
        }

        const productType = await ProductType.findById(sku)

        if (!productType) {
            throw HttpError(400, 'We could not find a product type with the given SKU!')
        }

        productType.dimension = productTypeDimension
        await productType.save()

        res.status(201).send()
    } catch (error) {
        next(error)
    }
})

/**
 * @route PUT /product/type/dimension
 * @desc Update the existing dimensions of a given product type
 */
router.put('/', async (req, res, next) => {
    try {
        const { sku, width, length, height, unit } = req.body
        const productTypeDimension = { width, length, height, unit } as DimensionInterface

        if (!sku) {
            throw HttpError(400, 'SKU is missing!')
        }

        const productType = await ProductType.findById(sku)

        if (!productType) {
            throw HttpError(400, 'We could not find a product type with the given SKU!')
        }

        await productType.updateOne({ dimension: productTypeDimension })

        res.status(204).send()
    } catch (error) {
        next(error)
    }
})

/**
 * @route DELETE /product/type/dimension
 * @desc Delete the existing dimensions from a given product type
 */
 router.delete('/', async (req, res, next) => {
    try {
        const { sku } = req.body

        if (!sku) {
            throw HttpError(400, 'SKU is missing!')
        }

        const productType = await ProductType.findById(sku)

        if (!productType) {
            throw HttpError(400, 'We could not find a product type with the given SKU!')
        }

        productType.set('dimension', undefined, { strict: false })

        await productType.save()

        res.send()
    } catch (error) {
        next(error)
    }
})

export { router };
