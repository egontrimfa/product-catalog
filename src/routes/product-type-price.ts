import { Router } from "express";
import HttpError from 'http-errors';
import { PriceInterface } from "../interfaces/Price";
import { ProductType } from "../models";

const router: Router = new (Router as any)()

/**
 * @route POST /product/type/price
 * @desc Add a new price to a given product type
 */
 router.post('/', async (req, res, next) => {
    try {
        const { sku, original, discount, currency } = req.body
        const productTypePrice = { original, discount, currency } as PriceInterface

        if (!sku) {
            throw HttpError(400, 'SKU is missing!')
        }

        const productType = await ProductType.findById(sku)
        
        if (!productType) {
            throw HttpError(400, 'We could not find a product type with the given SKU!')
        }

        productType.prices.push(productTypePrice)
        await productType.save()

        res.status(201).send()
    } catch (error) {
        next(error)
    }
})

/**
 * @route PUT /product/type/price
 * @desc Update an existing price of a product type
 */
 router.put('/', async (req, res, next) => {
    try {
        const { sku, priceID, original, discount, currency } = req.body
        const productTypePrice = { original, discount, currency } as PriceInterface

        if (!sku) {
            throw HttpError(400, 'SKU is missing!')
        }

        const productType = await ProductType.findById(sku)    

        if (!productType) {
            throw HttpError(400, 'We could not find a product type with the given SKU!')
        }

        const price = productType.prices.find(price => price._id == priceID)

        if (!price) {
            throw HttpError(400, 'The searched product type does not have the price with the given id!')
        }

        await ProductType.findOneAndUpdate(
            { "_id": sku, "prices._id": priceID },
            { "prices.$": { ...productTypePrice, _id: priceID } }
        )

        res.status(204).send()
    } catch (error) {
        next(error)
    }
})

/**
 * @route DELETE /product/type/price
 * @desc Delete an existing price of a product type
 */
 router.delete('/', async (req, res, next) => {
    try {
        const { sku, priceID } = req.body

        if (!sku) {
            throw HttpError(400, 'SKU is missing!')
        }

        const productType = await ProductType.findById(sku)    

        if (!productType) {
            throw HttpError(400, 'We could not find a product type with the given SKU!')
        }

        const price = productType.prices.find(price => price._id == priceID)

        if (!price) {
            throw HttpError(400, 'The searched product type does not have the price with the given id!')
        }

        await productType.updateOne({ $pull: { prices: { _id: priceID } } })

        res.send()
    } catch (error) {
        next(error)
    }
})

export { router };
