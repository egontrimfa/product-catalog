import { Router } from "express"
import { ProductTypeInterface } from "../interfaces/ProductType"
import { Product, ProductType } from "../models"
import { SKUGenerator } from "../utils/sku-generation"
import HttpError from 'http-errors';
import { MoveInterface } from "../interfaces";
import { notifySubscribedClients } from "../services";

const router: Router = new (Router as any)()

/**
 * @route POST /product/type
 * @desc Create a new product type for an existing product
 */
router.post('/', async (req, res, next) => {
    try {
        const { available, description, dimension, images, parent_sku, prices, stock } = req.body
        const productType = { _id: SKUGenerator(4, parent_sku), available, description, dimension, images, parent_sku, prices, stock } as ProductTypeInterface

        await ProductType.create(productType)

        return res.status(201).send()
    } catch (error) {
        next(error)
    }
})

/**
 * @route GET /product/type/list
 * @desc Get all product types by the provided parent SKU
 */
 router.get('/list', async (req, res, next) => {
    try {
        const { parent_sku } = req.body

        if (!parent_sku) {
            throw HttpError(400, 'Parent SKU is missing!')
        }

        const product = await Product.findById(parent_sku)

        if (!product) {
            throw HttpError(404, 'We could not find a product with the given parent SKU!')
        }

        const productTypes = await ProductType.find({ parent_sku })

        return res.send(productTypes.map(productType => productType.toObject({ versionKey: false })))
    } catch (error) {
        next(error)
    }
})

/**
 * @route GET /product/type/list/all
 * @desc Get all product types for all products
 */
 router.get('/list/all', async (req, res, next) => {
    try {
        const productTypes = await ProductType.find({}, '-createdAt -updatedAt -dimension._id -prices._id').populate('parent_sku', '-createdAt -updatedAt')

        return res.send(productTypes.map(productType => productType.toObject({ versionKey: false })))
    } catch (error) {
        next(error)
    }
})

/**
 * @route GET /product/type
 * @desc Get a product type by the provided SKU
 */
router.get('/:sku', async (req, res, next) => {
    try {
        const { sku } = req.params

        if (!sku) {
            throw HttpError(400, 'SKU is missing!')
        }

        const productType = await ProductType.findById(sku)

        if (!productType) {
            throw HttpError(404, 'We could not find a product type with the given SKU!')
        }

        return res.send(productType.toObject({ versionKey: false }))
    } catch (error) {
        next(error)
    }
})

/**
 * @route PUT /product/type
 * @desc Update an existing product type of a product
 */
router.put('/', async (req, res, next) => {
    try {
        const { sku, available, description, dimension, images, parent_sku, prices, stock } = req.body
        const productType = { _id: sku, available, description, dimension, images, parent_sku, prices, stock } as ProductTypeInterface

        if (!sku) {
            throw HttpError(400, 'SKU is missing!')
        }

        const oldProductType = await ProductType.findById(sku)

        if (!oldProductType) {
            throw HttpError(400, 'We could not find a product type with the given SKU!')
        }

        const _productType = Object.fromEntries(Object.entries(productType).filter(([_, value]) => value != null)) as ProductTypeInterface
        const _oldProductType = Object.fromEntries(Object.entries(oldProductType.toObject()).filter(
            ([key]) => [ '_id', 'available', 'description', 'dimension', 'images', 'parent_sku', 'prices', 'stock' ].includes(key)
        )) as ProductTypeInterface

        const newProductType = { ..._oldProductType, ..._productType } as ProductTypeInterface

        await oldProductType.updateOne(newProductType)

        if (oldProductType.stock === 0 && newProductType.stock > 0) {
            await notifySubscribedClients(newProductType._id, newProductType.description)
        }

        return res.status(204).send()
    } catch (error) {
        next(error)
    }
})

/**
 * @route /product/move
 * @desc take or give product types to/from the users's cart 
 */
router.put('/move', async (req, res, next) => {
    try {
        const { sku, quantity }: MoveInterface = req.body
    
        if (!sku) {
            throw HttpError(400, 'SKU is missing!')
        }
    
        if (!quantity) {
            throw HttpError(400, 'Quantity is missing!')
        }
    
        const productType = await ProductType.findById(sku)
    
        if (!productType) {
            throw HttpError(404, 'We could not find a product type with the given SKU!')
        }
    
        const modifiedStock = Number(productType.stock) + Number(quantity)
    
        await productType.updateOne({ stock: modifiedStock }, { runValidators: true })
    
        return res.status(204).send()
    } catch (error) {
        next(error)
    }
})

/**
 * @route DELETE /product/type
 * @desc Delete an existing product type of a pruduct
 */
 router.delete('/', async (req, res, next) => {
    try {
        const { sku } = req.body

        if (!sku) {
            throw HttpError(400, 'Parent SKU is missing!')
        }

        const productType = await ProductType.findById(sku)

        if (!productType) {
            throw HttpError(404, 'We could not find a product type with the given SKU!')
        }

        await productType.delete()

        return res.send()
    } catch (error) {
        next(error)
    }
})

export { router };