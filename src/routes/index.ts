import { Application } from 'express';
import { router as productRouter } from './product';
import { router as productTypeRouter } from './product-type';
import { router as ProductTypePriceRouter } from './product-type-price';
import { router as ProductTypeImageRouter } from './product-type-image';
import { router as ProductTypeDimensionRouter } from './product-type-dimension';

export const mountRoutes = (app: Application) => {
    app.use('/product', productRouter)
    app.use('/product/type', productTypeRouter)
    app.use('/product/type/price', ProductTypePriceRouter)
    app.use('/product/type/image', ProductTypeImageRouter)
    app.use('/product/type/dimension', ProductTypeDimensionRouter)
}