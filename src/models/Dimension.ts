import { model, Schema } from 'mongoose';
import { DimensionInterface } from '../interfaces/Dimension';

const DimensionSchema = new Schema({
    width: {
        type: Number,
        required: true
    },
    height: {
        type: Number,
        required: true
    },
    length: {
        type: Number,
        required: true
    },
    unit: {
        type: String,
        required: true
    }
})

export const Dimension = model<DimensionInterface>('Dimension', DimensionSchema)