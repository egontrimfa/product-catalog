import { model, Schema } from 'mongoose';
import { PriceInterface } from '../interfaces/Price';

const PriceSchema = new Schema({
    original: {
        type: Number,
        required: true
    },
    discount: {
        type: Number,
        required: true
    },
    currency: {
        type: String,
        required: true
    }
})

export const Price = model<PriceInterface>('Price', PriceSchema)