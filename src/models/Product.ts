import { model, Schema } from 'mongoose';
import { ProductInterface } from '../interfaces/Product';

const ProductSchema = new Schema({
    _id: String,
    title: {
        type: String,
        unique: true,
        required: true
    }
}, { timestamps: true })

export const Product = model<ProductInterface>('Product', ProductSchema)