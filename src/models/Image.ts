import { model, Schema } from 'mongoose';
import { ImageInterface } from '../interfaces/Image';

const ImageSchema = new Schema({
    source: {
        type: String,
        required: true
    },
    label: {
        type: String,
        required: true
    }
})

export const Image = model<ImageInterface>('Image', ImageSchema)