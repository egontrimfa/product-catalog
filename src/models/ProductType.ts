import { model, Schema } from 'mongoose';
import idValidator from 'mongoose-id-validator';

import { ProductTypeInterface } from '../interfaces/ProductType';
import { Dimension } from './Dimension';
import { Image } from './Image';
import { Price } from './Price';

const ProductTypeSchema = new Schema({
    _id: String,
    parent_sku: {
        type: String,
        ref: 'Product',
        required: true
    },
    available: {
        type: Boolean,
        required: true
    },
    stock: {
        type: Number,
        required: true,
        min: [0, 'Unsufficient stock!']
    },
    prices: [Price.schema],
    description: {
        type: String,
        required: true
    },
    dimension: Dimension.schema,
    images: [Image.schema]
}, { timestamps: true })

ProductTypeSchema.plugin(idValidator, { message: "The given parent SKU does not exists!" })

export const ProductType = model<ProductTypeInterface>('ProductType', ProductTypeSchema)
