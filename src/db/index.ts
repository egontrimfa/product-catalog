import mongoose from 'mongoose';

const { DB_PROTOCOL, DB_CONNECTION_URI, DB_USERNAME, DB_PASSWORD, DB_NAME, DB_CONNECTION_OPTIONS } = process.env

export const initializeDBConnection = async () => {
    try {
        await mongoose.connect(`${DB_PROTOCOL}://${DB_USERNAME}:${DB_PASSWORD}@${DB_CONNECTION_URI}/${DB_NAME}?${DB_CONNECTION_OPTIONS}`)
        console.log('Successfully connected to the PC DB (MongoDB)');
    } catch (error) {
        console.log(error);
    }
}