import { DimensionInterface } from "./Dimension";
import { ImageInterface } from "./Image";
import { PriceInterface } from "./Price";

export interface ProductTypeInterface {
    _id: String,
    parent_sku: String,
    available: Boolean,
    stock: Number,
    prices: PriceInterface[],
    description: String,
    dimension: DimensionInterface,
    images: ImageInterface[]
}