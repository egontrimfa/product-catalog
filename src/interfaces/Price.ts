import { ObjectId } from "mongodb"

export interface PriceInterface {
    _id: ObjectId,
    original: Number,
    discount: Number,
    currency: String
}