import { ObjectId } from "mongodb"

export interface ImageInterface {
    _id: ObjectId,
    source: String,
    label: String
}