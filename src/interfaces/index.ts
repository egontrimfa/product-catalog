export interface MoveInterface {
    sku: String,
    quantity: Number
}