export interface DimensionInterface {
    width: Number,
    height: Number,
    length: Number,
    unit: String
}