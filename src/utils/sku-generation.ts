export const SKUGenerator = (length: number = 8, base: string = ''): String => {
    let SKU = base
    const charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    const charsetLength = charset.length

    for (let index = 0; index < length; index++) {
        SKU += charset.charAt(Math.floor(Math.random() * charsetLength))    
    }

    return SKU
}