import axios from "axios";
import HttpError from 'http-errors';

const axiosErrorHandler = (error: any) => {
    if (error.response) {
        return HttpError(error.response.status, error.response.data?.error)
    }

    if (error.request) {
        return HttpError(500, 'No response was received.')
    }

    return HttpError(500, error.message)
}

export const notifySubscribedClients = async (sku: String, productTypeName: String) => {
    try {
        await axios.post('http://customernotification-service/notify', { sku, product_type_name: productTypeName })
    } catch (error: any) {
        throw axiosErrorHandler(error)
    }
} 